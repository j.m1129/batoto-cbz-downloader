# batoto-cbz-downloader
A small program to download chapters of manga from bato.to and convert it to .cbz.

Usage:
php downloader.php URLs [options]

# Options
--keep-files, -k - keeps downloaded files

--no-convert, -n - doesn't create .cbz archive

--till-last-chapter, -l - keeps downloading until the last chapter of series
